//
// Created by Александр Дьячков on 03.01.2022.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "../include/BMP_Format.h"
#include "../include/image.h"
#include <stdint.h>

typedef struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((__packed__)) HEADER;

HEADER generateHeader(const IMAGE* image);

uint8_t padding(uint64_t width);

uint32_t bmp_padding(const IMAGE* image);

uint32_t squareSize(const IMAGE* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_FORMAT_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_FORMAT_H
#include "../include/image.h"
#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp( FILE* in, IMAGE* image );

enum write_status to_bmp( FILE* out, const IMAGE* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_FORMAT_H

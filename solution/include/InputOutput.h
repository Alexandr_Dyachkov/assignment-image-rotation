#ifndef ASSIGNMENT_IMAGE_ROTATION_INPUTOUTPUT_H
#define ASSIGNMENT_IMAGE_ROTATION_INPUTOUTPUT_H

#include "../include/BMP.h"
#include <stdint.h>
#include <stdio.h>

enum FileReadStatus{
    FILE_READ_SUCCESS = 0,
    FILE_READ_ERROR,
    FILE_READ_PATH_NULL
};

enum FileWriteStatus{
    FILE_WRITE_SUCCESS = 0,
    FILE_WRITE_ERROR,
    FILE_WRITE_PATH_NULL
};

enum FileCloseStatus{
    FILE_CLOSE_SUCCESS = 0,
    FILE_CLOSE_ERROR
};

enum FileReadStatus read(FILE** file, const char* path);

enum FileWriteStatus write(FILE** file, const char* path);

enum FileCloseStatus closeFile(FILE** file);

#endif //ASSIGNMENT_IMAGE_ROTATION_INPUTOUTPUT_H

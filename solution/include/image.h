// Здесь объявляется структура изображения
#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include "../include/pixel.h"
#include <stdint.h>

typedef struct image {
    uint64_t width, height;
    PIXEL* data;
}IMAGE;

void imageClear(IMAGE* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

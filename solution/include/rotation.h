#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#include "../include/image.h"

/* создаёт копию изображения, которое повёрнуто на 90 градусов против часовой стрелки */
struct image rotate_image(const IMAGE* source );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATION_H

#include "../include/BMP.h"
#include <stdlib.h>

uint32_t bmp_padding(const IMAGE* image){
    uint32_t result = (image->width % 4)*image->height;
    return result;
}

uint32_t squareSize(const IMAGE* image){
    uint32_t result = image->height*image->width;
    return result;
}

HEADER generateHeader(const IMAGE* image){
    HEADER header = {
            .bfType = 0x4D42,
            .bfileSize = squareSize(image)*sizeof(PIXEL)+bmp_padding(image)*sizeof(PIXEL),
            .bfReserved = 0,
            .bOffBits = sizeof(HEADER),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = squareSize(image)*sizeof(PIXEL)+bmp_padding(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return header;
}
uint8_t padding(uint64_t width)
{
    return 4 - ((width * 3) % 4);
}

enum read_status from_bmp( FILE* in, IMAGE* image ){
    HEADER header = {0};
    if (fread(&header, sizeof(HEADER), 1, in) == 0) {
        return READ_INVALID_HEADER;
    }else if (header.bfType != 19778) {
        return READ_INVALID_SIGNATURE;
    }
    *image = (IMAGE){header.biWidth,header.biHeight,NULL};
    fseek(in, header.bOffBits, 0);
    PIXEL *data = malloc(squareSize(image) * sizeof(PIXEL));
    uint8_t calculatedPadding = padding(image->width);
    for (size_t row = 0; row < image->height; row=row+1){
        fread(&data[image->width * row], sizeof(*data), image->width, in);
        fseek(in, calculatedPadding, 1);
    }
    image->data = data;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, const IMAGE* image){
    HEADER header = generateHeader(image);
    if (fwrite(&header, sizeof(HEADER), 1, out) != 1) {
        return WRITE_ERROR;
    }
    fseek(out, header.bOffBits, 0);
    uint8_t calculatedPadding = padding(image->width);
    uint32_t padding_value = 0;
    for (size_t row = 0; row < image->height; row=row+1){
        size_t res1 = fwrite(&image->data[image->width * row], sizeof(PIXEL), image->width, out);
        if (res1 != image->width){
            return WRITE_ERROR;
        }
        size_t res2 = fwrite(&padding_value, 1, calculatedPadding, out);
        if (res2 != calculatedPadding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

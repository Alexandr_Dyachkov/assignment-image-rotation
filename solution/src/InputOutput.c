#include "../include/InputOutput.h"

// Здесь определяются функции для работы c IO данных

enum FileReadStatus read(FILE** file, const char* path){ //читаем файл с диска
    *file = fopen(path,"rb");
    if (path == NULL){
        return FILE_READ_PATH_NULL;
    } else if (*file == NULL) {
        return FILE_READ_ERROR;
    }else {
        return FILE_READ_SUCCESS;
    }
}

enum FileWriteStatus write(FILE** file, const char* path){ //открываем файл на диске
    *file = fopen(path, "wb");
    if (path == NULL){
        return FILE_WRITE_PATH_NULL;
    }else if (*file == NULL){
        return FILE_WRITE_ERROR;
    }else{
        return FILE_WRITE_SUCCESS;
    }
}

enum FileCloseStatus closeFile(FILE** file){ //закрываем файл и возвращаем успех/поражение
    if (*file != NULL) {
        fclose(*file);
    }else {
        return FILE_CLOSE_ERROR;
    }
    return FILE_CLOSE_SUCCESS;
}

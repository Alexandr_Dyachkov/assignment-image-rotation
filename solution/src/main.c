#include "../include/image.h"
#include "../include/readWrite.h"
#include "../include/rotation.h"

int main( int argc, char** argv ) {
    (void) argc;
    (void) argv;
    if (argc != 3) {
        return 1;
    }
    char *inputPath = argv[1],*outputPath = argv[2];
//    inputPath="/Users/alexdyachkov/CLionProjects/assignment-image-rotation/tester/tests/3/input.bmp";
//    outputPath="/Users/alexdyachkov/CLionProjects/assignment-image-rotation/tester/tests/3/tesst3.bmp";
    IMAGE imageIn = readFile(inputPath);

    IMAGE rotatedImage = rotate_image(&imageIn);

    writeFile(outputPath,imageIn, rotatedImage);

    return 0;
}

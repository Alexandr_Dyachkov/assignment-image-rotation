#include "../include/image.h"
#include "../include/InputOutput.h"
#include <stdio.h>

IMAGE readFile(char *inputPath) {
    IMAGE imageIn;
    FILE *file_in = {0};
    read(&file_in, inputPath);
    from_bmp(file_in, &imageIn);
    closeFile(&file_in);
    return imageIn;
}

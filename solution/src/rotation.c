#include "../include/rotation.h"
#include <stdlib.h>

IMAGE rotate_image(IMAGE const* source ) {
    IMAGE rotatedImage ={source->height,source->width,malloc(sizeof(PIXEL)*source->height*source->width)};
    for (size_t i = 0; i < source->width; ++i) {
        for (size_t j = 0; j < source->height; ++j) {
            rotatedImage.data[i* source->height+ (source->height-j-1)] = source->data[j * source->width+i];
        }
    }
    return rotatedImage;
}

#include "../include/image.h"
#include "../include/InputOutput.h"
#include <stdio.h>

void writeFile(char *outputPath,IMAGE imageIn,IMAGE imageOut) {
    FILE *file_out = fopen(outputPath, "wb");
    write(&file_out, outputPath);
    to_bmp(file_out, &imageOut);
    closeFile(&file_out);
    imageClear(&imageIn);
    imageClear(&imageOut);
}
